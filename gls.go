package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	var fmode string
	var ftype string
	dirname := "."
	/*
		if len(os.Args) == 2 {
			dirname = os.Args[1]
		}
	*/

	for j := len(os.Args) - 1; j >= 1; j-- {
		dirname = os.Args[j]
	}

	fstat, err := os.Stat(dirname)

	if err != nil {
		log.Fatal(err)
	}

	if fstat.IsDir() {
		files, err := ioutil.ReadDir(dirname)
		if err != nil {
			log.Fatal(err)
		}

		for _, f := range files {
			fmode = f.Mode().Perm().String()
			ftype = f.Name()

			i := len(fmode) - 1

			switch {
			case f.IsDir():
				ftype += "/"
			case fmode[i] == 'x':
				ftype += "*"
			}

			fmt.Printf("%s\n", ftype)
		}
	} else {
		fmt.Println(fstat.Name())
		//fmt.Println("don't be a noob, I WANT A MOTHERFUCKING DIRECTORY")
	}
}
